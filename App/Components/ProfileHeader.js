import React, {Component} from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {Thumbnail} from 'native-base';
import {Colors, Fonts} from '@common';
import {Icon} from 'react-native-elements';
import {XLText, RegularText} from '@Typography';
import Constants from '../Common/Constants';

const ProfileHeader = props => (
  <View style={styles.container}>
    <View style={styles.centerView}>
      <Thumbnail
        large
        style={styles.profileImageStyle}
        source={{uri: 'https://picsum.photos/300/300?grayscale'}}
      />
      <XLText textStyle={styles.name}>
        {props.user.firstName} {props.user.lastName}
      </XLText>
      <View style={styles.callView}>
        <RegularText>29 year old{'  '}|{'       '}</RegularText>
        <Icon name="call" size={20} />
        <RegularText textStyle={styles.textStyle}>{props.user.phone}</RegularText>
      </View>
    </View>
  </View>
);

export default ProfileHeader;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.red,
    alignItems: 'center',
    justifyContent: 'center',
  },
  centerView: {
    backgroundColor: 'white',
    borderRadius: 10,
    borderColor: Colors.lightGray,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: -60,
    marginTop: 60,
    width: Constants.screenWidth / 1.4,
    height: 150,
  },
  profileImageStyle: {
    marginTop: -50,
    marginBottom: 10,
  },
  name: {
    marginBottom: 10,
  },
  callView:{
    flexDirection: 'row',
    alignItems: 'center'
  },
  textStyle:{
    marginLeft: 5
  }
});
