import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, FlatList, StyleSheet,Text} from 'react-native';
import {List, ListItem, Right, Body} from 'native-base';
import {Icon} from 'react-native-elements';
import {SmallText, RegularText} from '@Typography';
import {Colors, GlobalStyles} from '@common';

class DropdownCell extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      valueId: null,
      name: "",
    };
  }

  _handlePress = () =>
    this.setState({
      expanded: !this.state.expanded,
    });

  _handleSelection = item => {
    this.props.onSelection(item);
    this.setState({valueId: item.id,name: item.name,  expanded: !this.state.expanded});
  };

  _keyExtractor = (item, index) => item.id.toString();

  _renderItem = ({item}) => {
    const {valueId} = this.state;
    return (
      <TouchableOpacity
        key={item.id}
        onPress={() => this._handleSelection(item)}
        style={styles.listItemStyle}>
        <RegularText
          bold={valueId === item.id}
          textStyle={
            valueId === item.id
              ? styles.itemTextSelectedStyle
              : styles.itemTextStyle
          }>
          {item.name}
        </RegularText>
      </TouchableOpacity>
    );
  };

  render() {
    const {dropdownItems, title, containerStyle} = this.props;
    const {expanded, name} = this.state;
    return (
      <List style={[styles.container, containerStyle]}>
        <ListItem
          noIndent
          underlayColor="transparent"
          onPress={() => this._handlePress()}>
          <Body>
            <Text style={styles.placeholder}>{title}</Text>
            <RegularText>{name}</RegularText>
          </Body>
          <Right>
            <Icon
              name={expanded ? 'chevron-up' : 'chevron-down'}
              color={Colors.mediumGray}
              type={GlobalStyles.Constants.iconType.materialCom}
            />
          </Right>
        </ListItem>
        {expanded && (
          <FlatList
            nestedScrollEnabled
            showsVerticalScrollIndicator
            style={[
              styles.flatListStyle,
              {height: dropdownItems.length > 5 ? 220 : null},
            ]}
            data={dropdownItems}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
          />
        )}
      </List>
    );
  }
}

DropdownCell.propTypes = {
  dropdownItem: PropTypes.object,
  title: PropTypes.string,
};

DropdownCell.defaultProps = {};

export default DropdownCell;

const styles = StyleSheet.create({
    container: {
        marginVertical: 20,
        marginHorizontal: 10
      },
      itemTextStyle: {
        color: Colors.mediumGray,
        textAlign: 'left',
      },
      itemTextSelectedStyle: {
        color: Colors.blue,
        textAlign: 'left',
      },
      listItemStyle: {
        paddingHorizontal: 8,
        paddingVertical: 8,
        borderRightWidth: 0.5,
        borderRightColor: Colors.mediumGray,
        borderLeftWidth: 0.5,
        borderLeftColor: Colors.mediumGray,
      },
      flatListStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: Colors.mediumGray,
      },
      placeholder:{
        marginVertical: 10,
        color: Colors.lightGray,
        marginHorizontal: 3,
    }
})
