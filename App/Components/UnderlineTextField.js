import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet} from 'react-native';
import {GlobalStyles, Fonts, Colors} from '@common';
import { Input } from 'react-native-elements';

class UnderlineTextField extends Component {
  render() {
    const {
      placeHolder,
      inputAccessoryViewID,
      onChange,
      onChangeText,
      autoCorrect,
      autoCapitalize,
      value,
      innerPlaceHolder,
      keyboardType,
      multiline,
      textInputStyle,
      containerStyle,
      editable,
      secureTextEntry,
    } = this.props;
    return (
          <Input
            inputContainerStyle={styles.inputContainerStyle}
            containerStyle={[styles.containerStyle ,containerStyle]}
            placeholder={placeHolder}
            editable={editable}
            value={value}
            inputStyle={[styles.textInput, textInputStyle]}
            autoCorrect={autoCorrect}
            autoCapitalize={autoCapitalize}
            keyboardType={keyboardType}
            onChangeText={onChangeText}
            onChange={onChange}
            multiline={multiline}
            secureTextEntry={secureTextEntry}
          />
      
    );
  }
}

UnderlineTextField.propTypes = {
  placeHolder: PropTypes.string,
  image: PropTypes.number,
  onChangeText: PropTypes.func,
  onPressRightIcon: PropTypes.func,
  showRightButton: PropTypes.bool,
  secureTextEntry: PropTypes.bool,
};
UnderlineTextField.defaultProps = {
  placeHolder: null,
  image: null,
  showRightButton: false,
  secureTextEntry: false,
};
export default UnderlineTextField;

const styles = StyleSheet.create({
  textInput: {
    color: 'black',
    fontSize: Fonts.size.medium,
  },
  errorStyle: {
    paddingHorizontal: 20,
    paddingTop: 5,
    color: 'red',
    fontSize: 10,
  },
  inputContainerStyle:{
    borderBottomColor: Colors.red,
  }
});
