import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import {Item, Input, Label} from 'native-base';
import {Icon} from 'react-native-elements';
import {GlobalStyles, Fonts, Colors} from '@common';
import {SmallText} from '@Typography';

class TextField extends Component {
  render() {
    const {
      placeHolder,
      rightIcon,
      inputAccessoryViewID,
      onChangeText,
      autoCorrect,
      autoCapitalize,
      showRightButton,
      onPressRightIcon,
      value,
      innerPlaceHolder,
      keyboardType,
      multiline,
      textInputStyle,
      containerStyle,
      editable,
      error,
      secureTextEntry,
    } = this.props;
    return (
      <>
        <Item>
          <Input
            placeholder={placeHolder}
            editable={editable}
            value={value}
            style={[styles.textInput, textInputStyle]}
            autoCorrect={autoCorrect}
            autoCapitalize={autoCapitalize}
            keyboardType={keyboardType}
            onChangeText={onChangeText}
            multiline={multiline}
            secureTextEntry={secureTextEntry}
          />
          {showRightButton && (
            <Icon
              name={rightIcon}
              type={GlobalStyles.Constants.iconType.materialCom}
              onPress={onPressRightIcon}
            />
          )}
        </Item>
        <Text style={styles.errorStyle}>{error}</Text>
      </>
    );
  }
}

TextField.propTypes = {
  placeHolder: PropTypes.string,
  image: PropTypes.number,
  onChangeText: PropTypes.func,
  onPressRightIcon: PropTypes.func,
  showRightButton: PropTypes.bool,
  secureTextEntry: PropTypes.bool,
};
TextField.defaultProps = {
  placeHolder: null,
  image: null,
  showRightButton: false,
  secureTextEntry: false,
};
export default TextField;

const styles = StyleSheet.create({
  textInput: {
    color: 'black',
    fontSize: Fonts.size.regular,
  },
  errorStyle: {
    paddingHorizontal: 20,
    paddingTop: 5,
    color: 'red',
    fontSize: 10,
  },
});
