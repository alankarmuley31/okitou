import React, {Component} from 'react';
import PropTypes, {string} from 'prop-types';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {Colors, Fonts} from '@common';
import {Card} from 'native-base';
import {CheckBox} from 'react-native-elements';
import {MediumText} from '@Typography';
import CustomListItem from './CustomListItem';
import {Languages} from '@common';

const CustomList = props => (
  <Card style={styles.checkBoxView}>
    {props.labels.map((label, index) => {
      return (
        <CustomListItem
          key={index}
          index={index}
          title={label.title}
          subTitle={label.subTitle}
        />
      );
    })}
  </Card>
);

CustomList.propTypes = {
  labels: PropTypes.arrayOf(Object).isRequired,
};

CustomList.defaultProps = {
  labels: [{}],
};

export default CustomList;

const styles = StyleSheet.create({
  checkBoxView: {
    borderRadius: 10,
  },
});
