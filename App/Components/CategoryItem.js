import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {Constants, Fonts, Images} from '@common';
import {LargeText, SmallText} from '@Typography';

const CategoryItem = props => {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(false);
  }, []);

  return (
    <TouchableOpacity style={styles.cardStyle} onPress={props.onPress}>
      <View style={styles.imageContainer}>
        {loading && <ActivityIndicator color="white" size="small" />}
        {!loading && (
          <Image
            source={{uri: `https://picsum.photos/200/300`}}
            style={styles.image}
          />
        )}
      </View>
      <View style={styles.container}>
        <LargeText textStyle={styles.textStyle}>{props.title}</LargeText>
      </View>
    </TouchableOpacity>
  );
};
export default CategoryItem;

const styles = StyleSheet.create({
  cardStyle: {
    flex: 1,
    marginHorizontal: 20,
    marginVertical: 10,
    borderRadius: 8,
    alignItems: 'flex-start',
    backgroundColor: 'white',
    // borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  textStyle: {
    fontFamily: Fonts.type.medium,
  },
  subTextStyle: {
    marginVertical: 8,
  },
  image: {
    flex: 1,
    height: '100%',
    width: '100%',
    borderRadius: 8,
  },
  container: {
    padding: 10,
    justifyContent: 'center',
    width: '100%',
    borderRadius: 8,
    backgroundColor: 'white',

  },
  imageContainer: {
    flex: 2,
    width: '100%',
  },
});
