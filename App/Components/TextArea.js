import React from 'react';
import {StyleSheet} from 'react-native';
import {Form, Textarea} from 'native-base';

const TextArea = props => {
  return (
    <Form>
      <Textarea
        rowSpan={props.rowSpan}
        bordered
        value={props.value}
        placeholder={props.placeholder}
        style={props.textAreaStyle}
        maxLength={props.textLength}
        onChangeText={props.onChangeText}
      />
    </Form>
  );
};
export default TextArea;
