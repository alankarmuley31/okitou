import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {Colors, Fonts} from '@common';
import {Body, Left, ListItem, Right} from 'native-base';
import {CheckBox, Icon} from 'react-native-elements';
import {MediumText} from '@Typography';

const CheckboxListItem = props => (
  <ListItem noIndent noBorder={props.radio}>
    <Left style={styles.leftStyle} >
      {props.radio && <Icon
        containerStyle={styles.leftIcon}
        reverse
        name="location-outline"
        type="ionicon"
        color={Colors.red}
        size={10}
      />}
      <MediumText textStyle={styles.checkBoxText}>{props.title}</MediumText>
    </Left>
    <Right>
      <CheckBox
        containerStyle={styles.checkBoxContainer}
        iconType="material"
        checkedIcon={props.radio ? 'radio-button-checked' : 'check-circle'}
        uncheckedIcon="radio-button-unchecked"
        checkedColor={Colors.blue}
        checked={props.checked}
        onPress={() => props.onPress(props.index)}
      />
    </Right>
  </ListItem>
);

CheckboxListItem.propTypes = {
  radio: PropTypes.bool,
  checked: PropTypes.bool,
  index: PropTypes.number,
  title: PropTypes.string,
};

CheckboxListItem.defaultProps = {
  radio: false,
  checked: false,
  index: 0,
  title: 'No title',
};

export default CheckboxListItem;

const styles = StyleSheet.create({
  checkBoxText: {
    color: Colors.mediumGray,
  },
  checkBoxContainer: {
    padding: 0,
    margin: 0,
  },
  leftStyle:{
    alignItems: 'center',
    paddingVertical: 5,
  },
  leftIcon:{
    marginRight: 10, 
    marginTop: 0,
    marginBottom: 0,
    padding: 0
  }
});
