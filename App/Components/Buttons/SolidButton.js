import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Colors, Fonts, GlobalStyle} from '@common';
import {MediumText} from '@Typography';
import {Button} from 'native-base';

const SolidButton = props => (
  <Button
    block
    rounded
    disabled={props.disabled}
    style={[
      styles.buttonStyle,
      props.disabled ? styles.disabled : null,
      props.buttonStyle,
    ]}
    onPress={props.onPress}>
    <MediumText bold textStyle={props.textStyle}>
      {props.title}
    </MediumText>
  </Button>
);

SolidButton.propTypes = {
  title: PropTypes.string.isRequired,
  showIcon: PropTypes.bool,
};
SolidButton.defaultProps = {
  title: 'Button',
  showIcon: false,
};

export default SolidButton;

const styles = StyleSheet.create({
  buttonStyle: {
    marginVertical: 10,
    height: 60,
    backgroundColor: Colors.red,
    marginHorizontal: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
  },
  disabled: {
    backgroundColor: Colors.lightGray,
  },
});
