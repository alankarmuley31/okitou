import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, Image} from 'react-native';
import {Colors, Fonts} from '@common';
import {SmallText} from '@Typography';
import {Button} from 'native-base';

const SocialIconButton = props => (
  <Button
    transparent
    style={[styles.buttonStyle, props.buttonStyle]}
    onPress={props.onPress}>
    <Image style={styles.logo} source={props.image} />
    <SmallText bold textStyle={[styles.textStyle, props.textStyle]}>
      {props.title}
    </SmallText>
  </Button>
);

SocialIconButton.propTypes = {
  title: PropTypes.string.isRequired,
};
SocialIconButton.defaultProps = {
  title: 'Button',
};

export default SocialIconButton;

const styles = StyleSheet.create({
  buttonStyle: {
    flexDirection: 'row',
    borderColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  textStyle: {
    color: Colors.blue,
  },
  logo: {width: 25, height: 25,marginHorizontal: 10 },
});
