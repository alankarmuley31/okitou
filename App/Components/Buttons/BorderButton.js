import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet} from 'react-native';
import {Colors, Fonts} from '@common';
import {MediumText} from '@Typography';
import {Button} from 'native-base';

const BorderButton = props => (
  <Button
    block
    rounded
    bordered
    style={[styles.buttonStyle, props.buttonStyle]}
    onPress={props.onPress}>
    <MediumText bold textStyle={props.textStyle}>
      {props.title}
    </MediumText>
  </Button>
);

BorderButton.propTypes = {
  title: PropTypes.string.isRequired,
};
BorderButton.defaultProps = {
  title: 'Button',
};

export default BorderButton;

const styles = StyleSheet.create({
  buttonStyle: {
    flexDirection: 'row',
    marginVertical: 10,
    borderColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    height: 60,
    marginHorizontal: 20,
  },
});
