import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Colors, Languages} from '@common';
import {RegularText} from '@Typography';
import {Content, Body, ListItem, Left} from 'native-base';
import {Icon} from 'react-native-elements';
import DetailCard from './DetailCard';
import CustomList from './CustomList';

import {SolidButton} from '@Buttons';

const Confirm = props => {
  var {aCourier} = props;

  const serviceConfirmationComponent = () => {
    if (!aCourier.ServiceSteps) {
      return null;
    }
    return (
      <View>
        <RegularText textStyle={styles.headingText}>
          {Languages.serviceConfirmation}
        </RegularText>
        <CustomList
          labels={[
            {title: 'Service Category', subTitle: aCourier.category.name},
            {title: 'Service Steps', subTitle: aCourier.ServiceSteps.name},
          ]}
        />
      </View>
    );
  };

  const serviceDetailComponent = () => {
    if (!aCourier.ServiceDetails) {
      return null;
    }
    return (
      <DetailCard
        showHeaderButton
        header={Languages.serviceDetail}
        detail={aCourier.ServiceDetails}
      />
    );
  };

  const customerAddressComponent = () => {
    if (!aCourier.address) {
      return null;
    }
    const addressText = aCourier.address.manual
      ? aCourier.address.manualAddress
      : `${aCourier.address.Number}  ${aCourier.address.Street}  ${aCourier.address.City} ${aCourier.address.Zip} \n ${aCourier.address.Instructions}`;
    return (
      <DetailCard showIcon header={Languages.customerAddress} detail={addressText} />
    );
  };

  const shippingElementComponent = () => {
    return <View />;
  };

  const pickupAddressComponent = () => {
    return (
      <DetailCard header={Languages.customerAddress} detail="Some Address" />
    );
  };

  const deliveryAddressComponent = () => {
    return <View />;
  };

  const elementsToBuyComponent = () => {
    return <View />;
  };

  return (
    <View style={styles.cardStyle}>
      <Content style={styles.cardStyle}>
        {serviceConfirmationComponent()}
        {serviceDetailComponent()}
        {customerAddressComponent()}
      </Content>
      <SolidButton title={Languages.proceed} onPress={props.onConfirm} />
      <SolidButton
        buttonStyle={styles.backButton}
        title={Languages.back}
        onPress={props.onBack}
      />
    </View>
  );
};
export default Confirm;

const styles = StyleSheet.create({
  cardStyle: {
    flex: 1,
    paddingHorizontal: 5,
  },
  checBoxView: {
    borderRadius: 10,
  },
  subText: {
    color: Colors.mediumGray,
    textAlign: 'left',
    paddingVertical: 10,
  },
  serviceDetailText: {
    color: Colors.mediumGray,
    padding: 15,
    textAlign: 'left',
  },
  headingText: {
    color: Colors.darkGray,
    textAlign: 'left',
    paddingVertical: 5,
  },
  serviceDetailHeadingView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textAreaStyle: {
    borderRadius: 10,
    paddingTop: 20,
    paddingLeft: 15,
    paddingBottom: 20,
    paddingRight: 15,
  },
  checkBoxContainer: {
    padding: 0,
    margin: 0,
  },
  backButton: {
    backgroundColor: Colors.lightGray,
  },
});
