import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Constants, Colors} from '@common';
import {RegularText} from '@Typography';
import {Card} from 'native-base';
import {useForm, Controller} from 'react-hook-form';
import TextArea from './TextArea';
import UnderlineTextField from './UnderlineTextField';
import CheckboxListItem from './CheckboxListItem';
import {Languages} from '@common';
import {SolidButton} from '@Buttons';

const Address = props => {
  var {aCourier} = props;
  const {control, handleSubmit, watch, formState} = useForm({
    mode: 'onChange',
  });
  var initialState = aCourier.address
    ? {
        Number: aCourier.address.Number,
        Street: aCourier.address.Street,
        City: aCourier.address.City,
        Zip: aCourier.address.Zip,
        Instructions: aCourier.address.Instructions,
        manualAddress: aCourier.address.manualAddress,
      }
    : {
        Number: '',
        Street: '',
        City: '',
        Zip: '',
        Instructions: '',
        manualAddress: '',
      };

  const watchIsManual = watch(
    'manual',
    aCourier.address ? aCourier.address.manual : true,
  );
  var rules = {
    required: !watchIsManual,
  };
  const onPressNext = data => {
    props.updateCourier({
      address: {
        ...data,
        manual: watchIsManual,
      },
    });
    props.onNext();
  };

  return (
    <View style={styles.cardStyle}>
      <View style={styles.cardStyle}>
        <RegularText textStyle={styles.headingText}>
          {Languages.customerAddress}
        </RegularText>
        <Card style={styles.checkBoxView}>
          <Controller
            control={control}
            render={({onChange, onBlur, value}) => (
              <CheckboxListItem
                radio
                title={Languages.manualAddress}
                checked={watchIsManual}
                onPress={() => {
                  onChange(true);
                  props.updateCourier({
                    address: {manual: true},
                  });
                }}
              />
            )}
            name="manual"
          />
          {watchIsManual && (
            <Controller
              control={control}
              rules={{required: watchIsManual}}
              render={({onChange, onBlur, value}) => (
                <TextArea
                  rowSpan={5}
                  value={value}
                  placeholder={Languages.address}
                  maxLength={Constants.maxTextLength}
                  textAreaStyle={styles.textAreaStyle}
                  onChangeText={text => {
                    onChange(text);
                  }}
                />
              )}
              name="manualAddress"
              defaultValue={initialState.manualAddress}
            />
          )}
          <Controller
            control={control}
            render={({onChange, onBlur, value}) => (
              <CheckboxListItem
                radio
                title={Languages.exactAddress}
                checked={!watchIsManual}
                onPress={() => {
                  onChange(false);
                  props.updateCourier({
                    address: {manual: false},
                  });
                }}
              />
            )}
            name="manual"
          />
          {!watchIsManual && (
            <>
              <View style={styles.addressContainer}>
                <Controller
                  control={control}
                  render={({onChange, onBlur, value}) => (
                    <UnderlineTextField
                      value={value}
                      placeHolder="No."
                      containerStyle={styles.noContainerStyle}
                      onChangeText={text => {
                        onChange(text);
                      }}
                    />
                  )}
                  name="Number"
                  rules={rules}
                  defaultValue={initialState.Number}
                />
                <Controller
                  control={control}
                  render={({onChange, onBlur, value}) => (
                    <UnderlineTextField
                      value={value}
                      placeHolder="Street"
                      containerStyle={styles.tfStreet}
                      onChangeText={text => {
                        onChange(text);
                      }}
                    />
                  )}
                  name="Street"
                  rules={rules}
                  defaultValue={initialState.Street}
                />
                <Controller
                  control={control}
                  render={({onChange, onBlur, value}) => (
                    <UnderlineTextField
                      value={value}
                      placeHolder="City"
                      containerStyle={styles.tfContainerStyle}
                      onChangeText={text => {
                        onChange(text);
                      }}
                    />
                  )}
                  name="City"
                  rules={rules}
                  defaultValue={initialState.City}
                />
                <Controller
                  control={control}
                  render={({onChange, onBlur, value}) => (
                    <UnderlineTextField
                      value={value}
                      placeHolder="Zip"
                      containerStyle={styles.tfContainerStyle}
                      onChangeText={text => {
                        onChange(text);
                      }}
                    />
                  )}
                  name="Zip"
                  rules={rules}
                  defaultValue={initialState.Zip}
                />
              </View>
              <Controller
                control={control}
                render={({onChange, onBlur, value}) => (
                  <TextArea
                    rowSpan={5}
                    value={value}
                    maxLength={Constants.maxTextLength}
                    placeholder={Languages.instruction}
                    textAreaStyle={styles.textAreaStyle}
                    onChangeText={text => {
                      onChange(text);
                    }}
                  />
                )}
                name="Instructions"
                rules={{required: false}}
                defaultValue={initialState.Instructions}
              />
            </>
          )}
        </Card>
      </View>
      <SolidButton
        title={Languages.next}
        onPress={handleSubmit(onPressNext)}
        disabled={!formState.isValid}
      />
      <SolidButton
        buttonStyle={styles.backButton}
        title={Languages.back}
        onPress={props.onBack}
      />
    </View>
  );
};
export default Address;

const styles = StyleSheet.create({
  cardStyle: {
    flex: 1,
  },
  checkBoxView: {
    borderRadius: 10,
  },
  headingText: {
    color: Colors.darkGray,
    textAlign: 'left',
    paddingVertical: 10,
  },
  textAreaStyle: {
    borderRadius: 10,
    paddingTop: 20,
    paddingLeft: 15,
    paddingBottom: 20,
    paddingRight: 15,
    margin: 20,
  },
  addressContainer: {
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  backButton: {
    backgroundColor: Colors.lightGray,
  },
  noContainerStyle: {
    flex: 0.6,
  },
  tfContainerStyle: {
    flex: 1,
  },
  tfStreet: {
    flex: 2,
  },
});
