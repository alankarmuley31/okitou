import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {Colors, Fonts} from '@common';
import {Body, Left, ListItem, Right} from 'native-base';
import {CheckBox, Icon} from 'react-native-elements';
import {MediumText, RegularText} from '@Typography';

const CustomListItem = props => (
  <ListItem noIndent>
    <Left style={styles.leftStyle}>
      <Body>
        <RegularText textStyle={styles.titleText}>{props.title}</RegularText>
        <MediumText textStyle={styles.subTitle}>{props.subTitle}</MediumText>
      </Body>
    </Left>
  </ListItem>
);

CustomListItem.propTypes = {
  index: PropTypes.number,
  title: PropTypes.string,
  subTitle: PropTypes.string,
};

CustomListItem.defaultProps = {
  index: 0,
  title: 'No title',
  subTitle: '',
};

export default CustomListItem;

const styles = StyleSheet.create({
  titleText: {
    color: Colors.darkGray,
  },
  subTitle: {
    color: Colors.mediumGray,
  },
  leftStyle: {
    // alignItems: 'center',
    // paddingVertical: 5,
  },
});
