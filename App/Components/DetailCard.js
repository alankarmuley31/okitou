import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Languages, Colors} from '@common';
import {Card, ListItem, Left} from 'native-base';
import {Icon} from 'react-native-elements';
import {RegularText, MediumText} from '@Typography';

const DetailCard = props => {
  return (
    <View>
      <View style={styles.headerContainer}>
        <RegularText textStyle={styles.headingText}>{props.header}</RegularText>
        {props.showHeaderButton && <Icon
          reverse
          name="alert-outline"
          type="ionicon"
          color={Colors.red}
          size={7}
        />}
      </View>
      <Card style={styles.card}>
        <ListItem noBorder>
          <Left>
            {props.showIcon && (
              <Icon
                reverse
                name="location-outline"
                type="ionicon"
                color={Colors.red}
                size={10}
              />
            )}
            <MediumText textStyle={styles.subText}>{props.detail}</MediumText>
          </Left>
        </ListItem>
      </Card>
    </View>
  );
};
export default DetailCard;

const styles = StyleSheet.create({
  card: {
    borderRadius: 10,
  },
  subText: {
    color: Colors.mediumGray,
    paddingVertical: 10,
  },
  headingText: {
    color: Colors.darkGray,
    textAlign: 'left',
    paddingVertical: 10,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
