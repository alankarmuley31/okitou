import React, {Component} from 'react';
import {
  Modal,
  Text,
  TouchableOpacity,
  View,
  Alert,
  TouchableHighlight,
  Platform,
  Image,
  StyleSheet,
} from 'react-native';
import {ListItem, Body, Right, Left} from 'native-base';
import {Images, Languages, Colors, Fonts} from '@common';
import {Picker} from '@react-native-community/picker';
import moment from 'moment';

class PickerModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      selectedItem: null,
    };
  }
  
  saveDate = () => {
    const {selectedDate, selectedTime} = this.state;
    var date = selectedDate ? selectedDate : this.state.date;
    if (selectedTime) {
      date = new Date(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        selectedTime.getHours(),
        selectedTime.getMinutes(),
        selectedTime.getSeconds(),
      );
    }
    this.setState({
      selectedDate: date,
      selectedTime: date,
      modalVisible: false,
    });
    this.props.setSelectedDate(date);
  };

  render() {
    const {
      modalVisible,
      selectedItem,
    } = this.state;
    const {placeholder, pickerItems} = this.props;

    const pickerItemsCells = pickerItems.map(aItem => <Picker.Item label={aItem} value={aItem} />)

    if (Platform.OS === 'ios') {
      return (
        <View style={styles.container}>
          <View style={styles.datePickerContainer}>
            <Image
              source={Images.calendarWhite}
              resizeMode="contain"
              style={styles.imageStyle}
            />
            <TouchableOpacity
              style={{flex: 1}}
              onPress={() =>
                this.setState({modalVisible: true})
              }>
              <Text style={styles.placeholder}>{placeholder}</Text>
              <Text style={styles.textStyle}>
                {selectedItem != null ? selectedItem : ''}
              </Text>
            </TouchableOpacity>
          </View>

          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <TouchableHighlight
              style={styles.backgroundContainer}
              onPress={() => this.setState({modalVisible: false})}>
              <View style={styles.pickerContainer}>
                <ListItem>
                  <Left>
                    <TouchableOpacity
                      onPress={() => this.setState({modalVisible: false})}>
                      <Text style={styles.buttonTextStyle}>Cancel</Text>
                    </TouchableOpacity>
                  </Left>

                  <Right>
                    <TouchableOpacity onPress={() => this.saveDate()}>
                      <Text style={styles.buttonTextStyle}>Done</Text>
                    </TouchableOpacity>
                  </Right>
                </ListItem>
                <View>
                <Picker
                  selectedValue= {selectedItem}
                  style={{width: '100%'}}
                  onValueChange={(itemValue, itemIndex) => this.setState({selectedItem: itemValue})}>
                  {pickerItemsCells}
                </Picker>
                </View>
              </View>
            </TouchableHighlight>
          </Modal>
        </View>
      );
    } else {
      return (
        <View>
          <View style={styles.datePickerContainer}>
            <Image
              source={Images.calendarWhite}
              resizeMode="contain"
              style={styles.imageStyle}
            />
            <TouchableOpacity
              style={{flex: 1}}
              onPress={() =>
                this.setState({modalVisible: true, pickerMode: 'date'})
              }>
              <Text style={styles.placeholder}>{placeholder}</Text>
              <Text style={styles.textStyle}>
                {selectedItem != null ? selectedItem : ''}
              </Text>
            </TouchableOpacity>
          </View>
          {modalVisible && (
            <Picker
              style={{width: '100%'}}
              selectedValue= {selectedItem}
              onValueChange={(itemValue, itemIndex) => this.setState({selectedItem: itemValue})}>
               {pickerItemsCells}
            </Picker>
          )}
        </View>
      );
    }
  }
}

PickerModal.defaultProps = {};

export default PickerModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  datePickerContainer: {
    borderBottomColor: Colors.lightGray,
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 10,
    marginLeft: 14,
    marginVertical: 10,
  },
  textStyle: {
    fontSize: Fonts.size.regular,
    color: Colors.darkGray,
    fontFamily: Fonts.type.regular,
  },
  backgroundContainer: {
    flex: 1,
    backgroundColor: Colors.semiTransparent,
    justifyContent: 'flex-end',
  },
  pickerContainer: {
    backgroundColor: 'white',
    paddingBottom: 30,
  },
  buttonTextStyle: {
    color: 'blue',
  },
  imageStyle: {
    height: 30,
  },
  placeholder: {
    marginVertical: 10,
    color: Colors.lightGray,
    marginHorizontal: 3,
  },
});
