import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {MediumText} from '@Typography';
import TextArea from './TextArea';
import {Languages, Colors, Constants} from '@common';
import {Icon} from 'react-native-elements';
import {SolidButton} from '@Buttons';
import CheckboxList from './CheckboxList';

const ServiceSelectionForm = props => {
  var {aCourier} = props;
  const label = [
    Languages.serviceFirst,
    Languages.clientFirst,
    Languages.customerService,
  ];
  const [currentSelectedItem, setCurrentSelectedItem] = useState(
    aCourier.ServiceSteps ? aCourier.ServiceSteps.id - 1 : null,
  );
  const [serviceDetail, setServiceDetail] = useState(
    aCourier.ServiceDetails ? aCourier.ServiceDetails : '',
  );
  const [enableNext, setEnableNext] = useState(
    currentSelectedItem !== null && serviceDetail.length > 0,
  );
  return (
    <View style={styles.cardStyle}>
      <View style={styles.cardStyle}>
        <MediumText textStyle={styles.headingText}>
          {Languages.serviceSteps}
        </MediumText>
        <CheckboxList
          labels={label}
          currentSelected={currentSelectedItem}
          onPress={index => {
            setEnableNext(serviceDetail.length > 0);
            setCurrentSelectedItem(index);
          }}
        />
        <View style={styles.serviceDetailHeadingView}>
          <MediumText textStyle={styles.headingText}>
            {Languages.serviceDetail}
          </MediumText>
          <Icon
            reverse
            name="alert-outline"
            type="ionicon"
            color={Colors.red}
            size={7}
          />
        </View>
        <TextArea
          rowSpan={5}
          placeholder={Languages.serviceDetail}
          textAreaStyle={styles.textAreaStyle}
          maxLength={Constants.maxTextLength}
          value={serviceDetail}
          onChangeText={text => {
            setServiceDetail(text);
            setEnableNext(currentSelectedItem !== null && text.length > 0);
          }}
        />
      </View>
      <SolidButton
        title={Languages.next}
        onPress={() =>
          props.onNext({
            ServiceSteps: {
              name: label[currentSelectedItem],
              id: currentSelectedItem + 1,
            },
            ServiceDetails: serviceDetail,
          })
        }
        disabled={!enableNext}
      />
    </View>
  );
};
export default ServiceSelectionForm;

const styles = StyleSheet.create({
  cardStyle: {
    flex: 1,
  },
  headingText: {
    color: Colors.darkGray,
    textAlign: 'left',
    paddingVertical: 10,
  },
  serviceDetailHeadingView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textAreaStyle: {
    borderRadius: 10,
    paddingTop: 20,
    paddingLeft: 15,
    paddingBottom: 20,
    paddingRight: 15,
    backgroundColor: Colors.white,
  },
});
