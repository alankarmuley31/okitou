import React, {Component} from 'react';
import { StyleSheet} from 'react-native';
import {Colors, Fonts} from '@common';
import {
  ListItem,
  Text,
  Left,
  Body,
} from 'native-base';

const ProfileItem = props => (
  <ListItem noBorder>
    <Left>
      <Text>{props.leftText}</Text>
    </Left>
    <Body style={styles.right}>
      <Text>{props.rightText}</Text>
    </Body>
  </ListItem>
);

export default ProfileItem;

const styles = StyleSheet.create({
  right: {
    flex: 2,
  },
});
