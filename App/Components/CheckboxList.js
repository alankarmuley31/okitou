import React, {Component} from 'react';
import PropTypes, {string} from 'prop-types';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {Colors, Fonts} from '@common';
import {Card} from 'native-base';
import {CheckBox} from 'react-native-elements';
import {MediumText} from '@Typography';
import CheckboxListItem from './CheckboxListItem';
import {Languages} from '@common';

const CheckboxList = props => (
  <Card style={styles.checkBoxView}>
    {props.labels.map((label, index) => {
      return (
        <CheckboxListItem
          key={index}
          index={index}
          title={label}
          checked={props.currentSelected === index}
          onPress={props.onPress}
        />
      );
    })}
  </Card>
);

CheckboxList.propTypes = {
  currentSelected: PropTypes.number,
  labels: PropTypes.arrayOf(string).isRequired,
};

CheckboxList.defaultProps = {
  currentSelected: 0,
  labels: [],
};

export default CheckboxList;

const styles = StyleSheet.create({
  checkBoxView: {
    borderRadius: 10,
  },
});
