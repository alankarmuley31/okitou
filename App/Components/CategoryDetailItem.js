import React, {useEffect, useState} from 'react';
import {Image, StyleSheet} from 'react-native';
import {Fonts} from '@common';
import {Card, CardItem, Text} from 'native-base';

const CategoryDetailItem = props => {
  return (
    <Card style={[styles.cardStyle, !props.item.IsActive ? styles.grayCardStyle: null]}>
      <CardItem button={props.item.IsActive}  onPress={props.onPress}>
        <Image
          source={{uri: props.item.CategoryIcon}}
          style={styles.image}
          resizeMode="contain"
        />
      </CardItem>
      <CardItem last>
        <Text style={styles.textStyle}>{props.item.CategoryName}</Text>
      </CardItem>
    </Card>
  );
};
export default CategoryDetailItem;

const styles = StyleSheet.create({
  cardStyle: {
    flex: 1,
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    marginBottom: 10,
  },
  grayCardStyle:{
    opacity: .2,
  },
  textStyle: {
    fontFamily: Fonts.type.medium,
    textAlign: 'center',
  },
  image: {
    height: 80,
    width: 80,
  },
});
