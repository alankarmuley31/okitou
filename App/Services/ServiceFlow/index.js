import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Container} from 'native-base';
import {View} from 'react-native';
import CourierActions from '../Home/reducer';
import ServiceSelectionForm from '../../Components/ServiceSelectionForm';
import Address from '../../Components/Address';
import Confirm from '../../Components/Confirm';
import styles from './styles';
import StepIndicator from 'react-native-step-indicator';
import {Languages, Constants} from '@common';
import {SolidButton} from '@Buttons';

const ServiceFlow = ({navigation}) => {
  const dispatch = useDispatch();
  const courier = useSelector(state => state.courier);
  const [stepperPosition, setStepperPosition] = useState(0);
  const [instruction, setInstruction] = useState('');
  const [address, setAddress] = useState('');
  const [serviceDetail, setServiceDetail] = useState('');

  const renderSteps = () => {
    switch (stepperPosition) {
      case 0:
        return (
          <ServiceSelectionForm
            aCourier={courier.aCourier}
            onNext={data => {
              dispatch(CourierActions.updateCourier(data));
              setStepperPosition(stepperPosition + 1);
            }}
          />
        );
      case 1:
        return (
          <Address
            aCourier={courier.aCourier}
            updateCourier= {data => {
              dispatch(CourierActions.updateCourier(data));
            }}
            onNext={() => {
              setStepperPosition(stepperPosition + 1)
            }}
            onBack={() => setStepperPosition(stepperPosition - 1)}
          />
        );
      case 2:
        return (
          <Confirm
            aCourier={courier.aCourier}
            onConfirm={() => {}}
            onBack={() => setStepperPosition(stepperPosition - 1)}
          />
        );
    }
  };

  return (
    <Container style={styles.containerStyle}>
      <View style={styles.stepIndicatorView}>
        <StepIndicator
          customStyles={Constants.stepperStyle}
          currentPosition={stepperPosition}
          labels={Constants.serviceSteps}
          stepCount={3}
        />
      </View>
      <View style={styles.formView}>{renderSteps()}</View>
    </Container>
  );
};
export default ServiceFlow;
