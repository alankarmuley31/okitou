import {post, get, put,patch, postMultipart, downloadFile, setToken} from '../genericApi';
import {getMock} from '../mockApi';

/* ------------- Global Functions  ------------- */

const getErrorText = error => error || 'Error completing request';

const checkError = res => {
  if (res.data.message) {
    throw new Error(getErrorText(res.data.message));
  }
};

/* ------------- Auth(User Profile) Functions  ------------- */

export const loginSocial = async payload => {
    const res = await post('/login/firebase', payload);
    checkError(res);
    return res;
};

export const login = async payload => {
   const res = await post('/login', payload);
   checkError(res);
   return res;
};

export const logout = async payload => {
  const res = await post('/logout', payload);
  checkError(res);
  return res;
};

export const register = async payload => {
  const res = await post('/register', payload);
   checkError(res);
   setToken(res.token);
   return res;
};

export const getProfile = async () => {
  const res = await get('/users/me');
  checkError(res);
  return res;
}


export const updateProfile = async (id , data) => {
  console.log()
  const res = await patch(`/users/${id}` ,data);
  checkError(res);
  return res;
}

/* ------------- Courier Functions  ------------- */

export const getServicesCategories = async () => {
  const res = await getMock('/7855494e-9bde-46e7-9102-47200e1f3f6f');
  checkError(res);
  return res;
}