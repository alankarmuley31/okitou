import {Alert} from 'react-native';

export const alert = async text => {
  Alert.alert(text);
};

export const confirm = async text => {
  const res = await new Promise(resolve => {
    Alert.alert(text, '', [
      {text: 'Confirmar', onPress: () => resolve(true)},
      {text: 'Cancelar', style: 'cancel', onPress: () => resolve(false)},
    ]);
  });

  return res;
};
