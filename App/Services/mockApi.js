import axios from 'axios';

const base_url = 'https://run.mocky.io/v3/'
const api = axios.create({
  baseURL: base_url,
  headers: {
    'Content-Type': 'application/json;charset=UTF-8',
    // 'Content-Type': 'multipart/form-data',
    //Accept: 'application/json',
  },
});

export const postMock = (endPoint, data) => {
  return new Promise((resolve, reject) => {
    api
      .post(endPoint, data)
      .then(res => {
        console.log(
          'API POST Response ------------------>   ' + JSON.stringify(res.data),
        );
        return resolve(res);
      })
      .catch(error => {
        console.log({...error}, 'Server error');
        return resolve(error.response);
      });
  });
};

export const getMock = (endPoint, data) => {
  return new Promise((resolve, reject) => {
    api
      .get(endPoint, {params: data})
      .then(res => {
        console.log(
          'API GET Response ------------------>   ' + JSON.stringify(res.data),
        );
        return resolve(res);
      })
      .catch(error => {
        console.log({...error}, 'Server error');
        return resolve(error.response);
      });
  });
};

export const putMock = (endPoint, data) => {
  return new Promise((resolve, reject) => {
    api
      .put(endPoint, data)
      .then(res => {
        console.log(
          'API PUT Response ------------------>   ' + JSON.stringify(res.data),
        );
        return resolve(res);
      })
      .catch(error => {
        console.log({...error}, 'Server error');
        return resolve(error.response);
      });
  });
};

export const patchMock = (endPoint, data) => {
  return new Promise((resolve, reject) => {
    api
      .patch(endPoint, data)
      .then(res => {
        console.log(
          'API PATCH Response ------------------>   ' + JSON.stringify(res.data),
        );
        return resolve(res);
      })
      .catch(error => {
        console.log({...error}, 'Server error');
        return resolve(error.response);
      });
  });
};

export const postMultipartMock = (endPoint, data) => {
  return new Promise((resolve, reject) => {
    axios({
      url: `${Config.apiUrl}/${endPoint}`,
      method: 'POST',
      data: data,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        Authorization: api.defaults.headers.common['Authorization'],
      },
    })
      .then(res => {
        console.log(
          'API POST Multipart Response ------------------>   ' +
            JSON.stringify(res.data),
        );
        return resolve(res);
      })
      .catch(error => {
        console.log({...error}, 'Server error');
        return resolve(error.response);
      });
  });
};

export const downloadFile = url => {
  return new Promise((resolve, reject) => {
    axios({
      url: url,
      method: 'GET',
      responseType: 'base64', //important
    }).then(({data}) => {
      const downloadUrl = window.URL.createObjectURL(new Blob([data]));
      const link = document.createElement('a');
      link.href = downloadUrl;
      link.setAttribute('download', 'file.zip'); //any other extension
      document.body.appendChild(link);
      link.click();
      link.remove();
    });
  });
};
