import {all} from 'redux-saga/effects';
import app from '../Screens/Root/saga';
import user from '../Screens/Profile/saga';
import courier from '../Screens/Home/saga';

export default function* sagas() {
  yield all([
    ...app,
    ...user,
    ...courier,
  ]);
}
