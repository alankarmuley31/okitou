import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  View,
  TouchableOpacity,
  Linking,
  KeyboardAvoidingView,
  Alert
} from 'react-native';
import {Container, Content, Button, Text, Form} from 'native-base';
import {CheckBox} from 'react-native-elements';

import TextField from '../../Components/TextField';
import {GlobalStyles, Languages} from '@common';
import styles from './styles';
import microValidator from 'micro-validator';
import is from 'is_js';
import Actions from '../Profile/reducer';
import {SolidButton, SocialIconButton} from '@Buttons';
import {SmallText, LargeText} from '@Typography';

const validationSchema = {
  first_name: {
    required: {
      errorMsg: 'First name is required',
    },
  },
  last_name: {
    required: {
      errorMsg: 'Last name is required',
    },
  },
  email: {
    required: {
      errorMsg: 'Email is required',
    },
    email: {
      errorMsg: 'Email is not valid',
    },
  },
  phone: {
    required: {
      errorMsg: 'Phone is required',
    },
  },
  password: {
    required: {
      errorMsg: 'Password is required',
    },
  },
  checkPassword: {
    required: {
      errorMsg: 'Please re enter password.',
    },
  },
};

const Register = ({navigation}) => {
  const dispatch = useDispatch();
  const [errors, setErrors] = useState({});
  const [formData, setFormData] = useState({});
  const [agreeTerms, setAgreeTerms] = useState(false);

  const onPressRegister = () => {
    const error = microValidator.validate(validationSchema, formData);
    if (!is.empty(error)) {
      setErrors(error);
      return;
    }
    if (!agreeTerms){
      Alert.alert(Languages.checkAgreeTerms)
      return
    }
    dispatch(
      Actions.register({
        firstName: formData.first_name,
        lastName: formData.last_name,
        email: formData.email,
        phone: formData.phone,
        password: formData.password,
        checkPassword: formData.checkPassword,
      }),
    );
  };

  const handleChange = (key, text) => {
    formData[key] = text;
    setFormData(formData);
    setErrors({});
  };

  return (
    <Container>
      <KeyboardAvoidingView
        style={GlobalStyles.style.flex1}
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        keyboardVerticalOffset={Platform.OS == 'ios' ? 0 : 20}
        enabled={Platform.OS === 'ios' ? true : false}>
        <Content padder>
          <Form style={styles.form}>
            <LargeText textStyle={styles.headerText}>
              {Languages.create_new_account}
            </LargeText>
            <TextField
              placeHolder={Languages.first_name}
              onChangeText={text => handleChange('first_name', text)}
              value={formData.first_name}
              error={errors.first_name && errors.first_name[0]}
            />
            <TextField
              placeHolder={Languages.last_name}
              onChangeText={text => handleChange('last_name', text)}
              value={formData.last_name}
              error={errors.last_name && errors.last_name[0]}
            />
            <TextField
              placeHolder={Languages.phone}
              onChangeText={text => handleChange('phone', text)}
              value={formData.phone}
              error={errors.phone && errors.phone[0]}
            />
            <TextField
              placeHolder={Languages.email}
              onChangeText={text => handleChange('email', text)}
              value={formData.email}
              error={errors.email && errors.email[0]}
            />
            <TextField
              placeHolder={Languages.password}
              onChangeText={text => handleChange('password', text)}
              value={formData.password}
              error={errors.password && errors.password[0]}
              secureTextEntry={true}
            />
            <TextField
              placeHolder={Languages.re_enter_password}
              onChangeText={text => handleChange('checkPassword', text)}
              value={formData.re_enter_password}
              error={errors.checkPassword && errors.checkPassword[0]}
              secureTextEntry={true}
            />
            <View style={styles.checkBoxView}>
              <CheckBox
                containerStyle={styles.checkBoxContainer}
                textStyle={styles.checkBoxTextStyle}
                checked={agreeTerms}
                onPress={() => setAgreeTerms(!agreeTerms)}
              />
              <TouchableOpacity onPress={() => Linking.openURL('https://google.com')}>
                <SmallText>{Languages.agree_terms}</SmallText>
              </TouchableOpacity>
            </View>
            <SolidButton
              buttonStyle={styles.loginBtn}
              title={Languages.register}
              onPress={onPressRegister}
            />
          </Form>
        </Content>
        <View style={styles.btmView}>
          {/* <View style={styles.curve} /> */}
          <TouchableOpacity
            style={styles.textContainer}
            onPress={() => navigation.navigate('Login')}>
            <SmallText style={styles.register_here}>
              {Languages.already_account}
              <SmallText bold style={styles.register_here}>
                {'  '}
                {Languages.small_sign_in}
              </SmallText>
            </SmallText>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </Container>
  );
};
export default Register;
