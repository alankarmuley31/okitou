import {StyleSheet} from 'react-native';
import {Colors, Fonts, Constants} from '@common';

export default StyleSheet.create({
    loginBtn:{
        marginTop: 30,
    },
    form:{
        paddingHorizontal: 15,
        marginBottom: 20,
    },
    forgotPass:{
        marginHorizontal: 10
    },
    btmView:{
        backgroundColor: Colors.blue,
        paddingBottom: 30,
        paddingTop: 10,
        height: 60,
        alignItems: 'center',
    },
    textContainer:{
        position: 'absolute'
    },
    register_here: {
        paddingTop: 15,
        color: Colors.white,
    },
    headerText:{
        color: Colors.mediumText,
        marginVertical: 10,
    },
    curve: {
        width: Constants.screenWidth / 2 + 30,
        height: Constants.screenWidth / 2,
        marginTop: -Constants.screenWidth / 4 + 10,
        backgroundColor: Colors.blue,
        borderRadius: Constants.screenWidth / 4,
        transform: [{scaleX: 2.8}, {scaleY: 0.5}],
      },
      checkBoxContainer:{
          backgroundColor: 'transparent',
          borderWidth: 0,
          marginBottom: 1,
      },
      checkBoxTextStyle:{
          color: Colors.mediumGray,
          fontSize: 12,
      },
      checkBoxView:{
          flexDirection: 'row',
          alignItems: 'center'
      }
});