import {call, put, takeLatest, select} from 'redux-saga/effects';
import {Platform} from 'react-native';
import Actions, {UserTypes, selectUser} from './reducer';
import AppActions, {app} from '../Root/reducer';
import CourierActions from '../Home/reducer';
import OkitouApi from '../../Services/OkitouApi';
import {setToken} from '../../Services/genericApi';
import {googleLogin, facebookLogin} from '../../Utils/auth';
import {navigate, popToTop, goBack} from '../../Utils/rNavigation';
import {Languages} from '@common';
import {decomposeFullName} from '../../Utils/strings';

function* google() {
  try {
    const res = yield googleLogin(false);
    if (res) {
      yield put(AppActions.loading(true));
      const {data} = yield call(OkitouApi.loginSocial, {
        "firebaseToken" : res
      })
      if (data) {
        setToken(data.token);
        yield put(
          Actions.setUser({
            auth: true,
            auth_token: data.token,
            ...data,
          }),
        );
        yield call(navigate, 'MainTab');
      }
      yield put(AppActions.loading(false));
    }
  } catch (e) {
    yield put(AppActions.error(e));
  }
}

function* facebook() {
  try {
    const res = yield facebookLogin(false);
    if (res) {
      yield put(AppActions.loading(true));
      const {data} = yield call(OkitouApi.loginSocial, {
        "firebaseToken" : res
      })
      if (data) {
        setToken(data.token);
        yield put(
          Actions.setUser({
            auth: true,
            auth_token: data.token,
            ...data,
          }),
        );
        yield call(navigate, 'MainTab');
      }
      yield put(AppActions.loading(false));
    }
  } catch (e) {
    yield put(AppActions.error(e));
  }
}

function* login(res) {
  try {
    const {data} = yield call(OkitouApi.login, res.payload);
    if (data) {
      setToken(data.token);
      yield put(
        Actions.setUser({
          auth: true,
          auth_token: data.token,
          ...data,
        }),
      );
      yield getProfile();
    yield call(navigate, 'MainTab');
    }
  } catch (e) {
    yield put(AppActions.error(e));
  }
}

function* register(res) {
  try {
    const {data} = yield call(OkitouApi.register, res.payload);
    if (data) {
      setToken(data.token);
      yield put(
        Actions.setUser({
          auth: true,
          auth_token: data.token,
          ...data,
        }),
      );
    yield getProfile();
    yield call(navigate, 'MainTab');
    }
  } catch (e) {
    yield put(AppActions.error(e));
  }
}

function* getProfile() {
  try {
    const {data} = yield call(OkitouApi.getProfile, null );
    if (data) {
      yield put(
        Actions.setUser({
          ...data,
        }),
      );
      //yield put(AppActions.showAlert(Languages.validate));

    }
  } catch (e) {
    yield put(AppActions.error(e));
  }
}

function* getProfileCheckValidity(payload) {
  switch (payload.serviceType.type.name) {
    case 'Shopping':
      
      break;
    case 'Transport':
        yield call(navigate, 'TransportFlow');
      break;
    case 'Services':
        yield call(navigate, 'CategoryDetail');
      break ; 
    default:
      break;
  }

  // const {data} = yield call(OkitouApi.getProfile, null );
  // if (data) {
  //   if (!data.valid) {
  //     yield put(AppActions.showAlert(Languages.validate));
  //   } else {
  //     yield put(CourierActions.updateCourier(payload.serviceType))
  //     yield call(navigate, 'CategoryDetail');
  //   }
  // }
}


function* updateProfile(payload){
  try {
    const { id } = yield select(selectUser)

    const {data} = yield call(OkitouApi.updateProfile, id, payload.profile);
    if (data) {
      yield put(
        Actions.setUser({
          ...data,
        }),
      );
    }
    yield call(goBack);
  } catch (e) {
    yield put(AppActions.error(e));
  }
}



function* logout() {
  try {
    yield call(OkitouApi.logout, null );
    yield put(Actions.resetUser());
    yield call(popToTop);
  } catch (e) {
    yield put(AppActions.error(e));
  }
}


export default [
  takeLatest(UserTypes.FACEBOOK, facebook),
  takeLatest(UserTypes.GOOGLE, google),
  takeLatest(UserTypes.LOGOUT, logout),
  takeLatest(UserTypes.LOGIN, login),
  takeLatest(UserTypes.REGISTER, register),
  takeLatest(UserTypes.GET_PROFILE, getProfile),
  takeLatest(UserTypes.UPDATE_PROFILE, updateProfile),
  takeLatest(UserTypes.GET_PROFILE_CHECK_VALIDITY , getProfileCheckValidity)
];
