import {StyleSheet} from 'react-native';
import {Colors, Fonts, Constants} from '@common';

export default StyleSheet.create({
  btmContainer: {
    flex: 5,
  },
  editIconContainer: {
    height: 60,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginBottom: 30,
  },
  address: {
    marginLeft: 30,
    width: '60%',
  },
  addressText: {
    flex: 1,
    marginTop: 20,
    alignSelf: 'flex-start',
    marginLeft: 20,
  },
});
