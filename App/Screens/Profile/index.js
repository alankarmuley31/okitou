import React, {useEffect, useLayoutEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View} from 'react-native';
import {Icon} from 'react-native-elements';
import {GlobalStyles, Constants, Languages} from '@common';
import ProfileHeader from '../../Components/ProfileHeader';
import styles from './styles';
import ProfileItem from '../../Components/ProfileItem';
import {Container, Content, List, ListItem, Text} from 'native-base';
import Actions from '../Profile/reducer';
import {XLText, RegularText} from '@Typography';

const Profile = ({navigation}) => {
  const user = useSelector(state => state.user);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Icon
          onPress={logout}
          name="logout"
          size={30}
          containerStyle={GlobalStyles.style.icon}
          type={Constants.mcIcon}
        />
      ),
    });
  }, [navigation]);

  const dispatch = useDispatch();
  useEffect(() => {});

  const logout = () => {
    dispatch(Actions.logout());
  };

  return (
    <Container>
      {user.auth && <ProfileHeader user={user} />}
      {user.auth && (
        <View style={styles.btmContainer}>
          <View style={styles.editIconContainer}>
            <Icon
              onPress={() => navigation.navigate('EditProfile')}
              name="square-edit-outline"
              size={30}
              containerStyle={GlobalStyles.style.icon}
              type={Constants.mcIcon}
            />
          </View>
          <Content>
            <List>
              <ListItem itemHeader noBorder />
              <ProfileItem leftText={Languages.email} rightText={user.email} />
              <RegularText textStyle={styles.addressText}>Address</RegularText>
              <View style={styles.address}>
                <ProfileItem
                  leftText={Languages.number}
                  rightText={user.address ? user.address.number : ''}
                />
                <ProfileItem
                  leftText={Languages.street}
                  rightText={user.address ? user.address.street : ''}
                />
                <ProfileItem
                  leftText={Languages.city}
                  rightText={user.address ? user.address.city : ''}
                />
                <ProfileItem
                  leftText={Languages.zip}
                  rightText={user.address ? user.address.zipCode : ''}
                />
              </View>
            </List>
          </Content>
        </View>
      )}
    </Container>
  );
};
export default Profile;
