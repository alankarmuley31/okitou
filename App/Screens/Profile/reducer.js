import {createActions, createReducer} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  setUser: ['payload'],
  resetUser: ['payload'],
  facebook: null,
  google: null,
  login: ['payload'],
  logout: ['payload'],
  register: ['payload'],
  getProfile: null,
  updateProfile: ['profile'],
  getProfileCheckValidity: ['serviceType'],
//   setProfile: ['payload'],
});

export const UserTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

const INITIAL_STATE = Immutable({
  auth: false,
  auth_token: null,
});

// /* ------------- Reducers ------------- */

const set = (state, {payload}) => state.merge(payload);
const reset = () => INITIAL_STATE;
//const setProfile = (state, {payload}) => state.set('profile', payload);

// /* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_USER]: set,
  [Types.RESET_USER]: reset,
//  [Types.SET_PROFILE]: setProfile,
});

export const selectUser = state => state.user;
