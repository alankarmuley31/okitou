import {StyleSheet} from 'react-native';
import {Colors} from '@common';

export default StyleSheet.create({
    containerStyle:{
        backgroundColor:Colors.offWhite,
        padding: 10,
    }
})