import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Container} from 'native-base';
import {FlatList} from 'react-native';
import CourierActions from '../Home/reducer';
import CategoryDetailItem from '../../Components/CategoryDetailItem';
import styles from './styles';

const CategoryDetail = ({navigation}) => {
  const dispatch = useDispatch();
  const courier = useSelector(state => state.courier);

  useEffect(() => {
    // TO DO call the actual api with below method
    //dispatch(CourierActions.getServicesCategories());
  });

  const renderItem = ({item}) => {
    return <CategoryDetailItem item={item} onPress={() => onPressCategory(item)} />;
  };
  const onPressCategory = (item) => {
    dispatch(CourierActions.updateCourier({category: {
      id: item.CategoryId,
      name: item.CategoryName,
    }}));
    navigation.navigate('ServiceFlow');
  };
  return (
    <Container style={styles.containerStyle}>
      <FlatList
        data={courier.servicesCategories}
        renderItem={renderItem}
        keyExtractor={item => item.CategoryId}
        numColumns={2}
      />
    </Container>
  );
};
export default CategoryDetail;
