import {StyleSheet} from 'react-native';
import {Colors, Fonts, Constants} from '@common';

export default StyleSheet.create({
    logoContainer: {
        height: Constants.screenHeight/4,
        alignItems:'center',
        justifyContent: 'center',
    },
    loginBtn:{
        marginTop: 30,
    },
    form:{
        paddingHorizontal: 10
    },
    forgotPass:{
        marginHorizontal: 18
    },
    btmView:{
        backgroundColor: Colors.blue,
        paddingBottom: 30,
        paddingTop: 10,
        height: 50,
        alignItems: 'center',
    },
    register_here: {
        color: Colors.white,
    },
    curve: {
        width: Constants.screenWidth / 2 + 30,
        height: Constants.screenWidth / 2,
        marginTop: -Constants.screenWidth / 4 + 10,
        backgroundColor: Colors.blue,
        borderRadius: Constants.screenWidth / 4,
        transform: [{scaleX: 2.8}, {scaleY: 0.5}],
      },
      textContainer:{
        position: 'absolute'
    },
});