import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  View,
  TouchableOpacity,
  Linking,
  KeyboardAvoidingView,
} from 'react-native';
import {Container, Content, Button, Text, Thumbnail, Form} from 'native-base';
import {GlobalStyles, Languages, Images} from '@common';
import {SolidButton, SocialIconButton} from '@Buttons';
import {SmallText} from '@Typography';
import microValidator from 'micro-validator';
import is from 'is_js';
import TextField from '../../Components/TextField';
import styles from './styles';
import Actions from '../Profile/reducer';

const validationSchema = {
  email: {
    required: {
      errorMsg: 'Email is required',
    },
    email: {
      errorMsg: 'Email is not valid',
    },
  },
  password: {
    required: {
      errorMsg: 'Password is required',
    },
  },
};

const Login = ({navigation}) => {
  const dispatch = useDispatch();
  const [errors, setErrors] = useState({});
  const [formData, setFormData] = useState({});
  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {});
  const onPressLogin = () => {
    const error = microValidator.validate(validationSchema, formData);
    if (!is.empty(error)) {
      setErrors(error);
      return;
    }
    dispatch(
      Actions.login({email: formData.email, password: formData.password}),
    );
  };
  const onPressGoogle = () => {
    dispatch(Actions.google());
  };
  const onPressFacebook = () => {
    dispatch(Actions.facebook());
  };

  const handleChange = (key, text) => {
    formData[key] = text;
    setFormData(formData);
    setErrors({});
  };

  return (
    <Container>
      <KeyboardAvoidingView
        style={GlobalStyles.style.flex1}
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        keyboardVerticalOffset={Platform.OS == 'ios' ? 0 : 20}
        enabled={Platform.OS === 'ios' ? true : false}>
        <Content padder>
          <View style={styles.logoContainer}>
            <Thumbnail
              large
              square
              source={{uri: 'https://picsum.photos/300/300?grayscale'}}
            />
          </View>
          <Form style={styles.form}>
            <TextField
              placeHolder={Languages.email}
              onChangeText={text => handleChange('email', text)}
              value={formData.email}
              error={errors.email && errors.email[0]}
            />
            <TextField
              placeHolder={Languages.password}
              onChangeText={text => handleChange('password', text)}
              value={formData.password}
              error={errors.password && errors.password[0]}
              secureTextEntry={!showPassword}
              showRightButton
              rightIcon={showPassword ? 'eye-off' : 'eye'}
              onPressRightIcon={() => setShowPassword(!showPassword)}
            />
            <SolidButton
              buttonStyle={styles.loginBtn}
              title={Languages.sign_in}
              onPress={onPressLogin}
            />
            <Button
              transparent
              style={styles.forgotPass}
              onPress={() => Linking.openURL('https://google.com')}>
              <SmallText>{Languages.forgot_password}</SmallText>
            </Button>
            <SocialIconButton
              title={Languages.google}
              image={Images.google}
              onPress={onPressGoogle}
            />
            <SocialIconButton
              title={Languages.facebook}
              image={Images.facebook}
              onPress={onPressFacebook}
            />
          </Form>
        </Content>
        <View style={styles.btmView}>
          <View style={styles.curve} />
          <TouchableOpacity
            style={styles.textContainer}
            onPress={() => navigation.navigate('Register')}>
            <SmallText style={styles.register_here}>
              {Languages.no_account}
              <SmallText bold style={styles.register_here}>
                {'  '}
                {Languages.register_here}
              </SmallText>
            </SmallText>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </Container>
  );
};
export default Login;
