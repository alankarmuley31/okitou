import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View} from 'react-native';
import {Container, Content, Button, Text, Thumbnail} from 'native-base';
import {SolidButton, BorderButton} from '@Buttons';
import {GlobalStyle, Languages} from '@common';
import styles from './styles';

const Introduction = ({navigation}) => {
  const dispatch = useDispatch();
  useEffect(() => {});
  return (
    <Container>
      <View style={styles.topContainer}>
        <Thumbnail
          large
          square
          source={{uri: 'https://picsum.photos/300/300?grayscale'}}
        />
      </View>
      <View style={styles.bottomContainer}>
        <SolidButton
          buttonStyle={styles.loginBtn}
          title={Languages.sign_in}
          onPress={() => navigation.navigate('Login')}
        />
        <BorderButton
          buttonStyle={styles.registerBtn}
          title={Languages.register}
          onPress={() => navigation.navigate('Register')}
        />
      </View>
    </Container>
  );
};
export default Introduction;
