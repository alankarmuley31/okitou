import {StyleSheet} from 'react-native';
import {Colors, Fonts, Constants} from '@common';

export default StyleSheet.create({
  topContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
  },
  bottomContainer: {
    flex: 1,
    backgroundColor: Colors.blue,
    paddingHorizontal: 30,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  loginBtn: {
   // marginTop: -Constants.screenWidth / 2 + 30,
  },
  registerBtn: {
    borderColor: 'white',
    borderWidth: 3,
  },
  midContainer: {
    flex: 0.2,
    backgroundColor: 'orange',
    alignItems: 'center',
  },
  curve: {
    width: Constants.screenWidth / 2 + 30,
    height: Constants.screenWidth / 2,
    marginTop: -Constants.screenWidth / 4 + 10,
    backgroundColor: Colors.blue,
    borderRadius: Constants.screenWidth / 4,
    transform: [{scaleX: 2.5}, {scaleY: 0.5}],
  },
});
