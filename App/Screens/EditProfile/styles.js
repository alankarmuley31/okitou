import {StyleSheet} from 'react-native';
import {Colors, Fonts, Constants} from '@common';

export default StyleSheet.create({
  topContainer: {
    paddingBottom: 20,
    paddingHorizontal: 20,
    backgroundColor: Colors.red,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  userName:{
      marginLeft: 10
  },
  save: {
    marginBottom: 20,
  },
  address:{
    marginLeft: 30,
    width: '60%'
  },
  addressInput:{
    flex: 2
  },
  addressText:{
    flex:1,
    marginTop: 20,
    alignSelf: 'flex-start',
    marginLeft: 20
  },
  passContainer:{paddingTop: 5, paddingBottom: 0 },
  left:{flex: 1},
  passItem: {flex: 1, marginTop: 0 ,marginBottom: 0, paddingTop: 0 , paddingBottom: 0 }
});
