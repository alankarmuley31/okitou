import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, Linking, StyleSheet} from 'react-native';
import {
  Container,
  Content,
  Form,
  ListItem,
  Left,
  Right,
  Item,
  Label,
  Input,
  Thumbnail,
} from 'native-base';
import {useForm, Controller} from 'react-hook-form';
import {Icon} from 'react-native-elements';
import {XLText, RegularText} from '@Typography';
import {SolidButton} from '@Buttons';
import DatePickerModal from '../../Components/DatePickerModal';
import PickerModal from '../../Components/PickerModal';
import DropdownCell from '../../Components/DropdownCell';
import {GlobalStyles, Languages, Constants} from '@common';
import Actions from '../Profile/reducer';
import styles from './styles';
import moment from 'moment';
import RNPickerSelect from 'react-native-picker-select';

const EditProfile = ({navigation}) => {
  const {control, handleSubmit, errors} = useForm();

  const user = useSelector(state => state.user);
  const initialState = {
    phone: user.phone ? user.phone.toString() : '',
    number: user.address && user.address.number ? user.address.number : '',
    street: user.address && user.address.street ? user.address.street : '',
    city: user.address && user.address.city ? user.address.city : '',
    zipCode:
      user.address && user.address.zipCode
        ? user.address.zipCode.toString()
        : '',
    birthdate: moment(user.birthdate),
    civility: user.civility,
  };
  const dispatch = useDispatch();
  const onPressSave = data => {
    const updatedProfile = {
      phone: data.phone,
      birthdate: data.birthdate,
      civility: data.civility,
      address: {
        number: data.number,
        street: data.street,
        city: data.city,
        zipCode: parseInt(data.zipCode),
      },
    };
    dispatch(Actions.updateProfile(updatedProfile));
  };

  return (
    <Container>
      <View style={styles.topContainer}>
        <Thumbnail
          large
          source={{uri: 'https://picsum.photos/300/300?grayscale'}}
        />
        <XLText textStyle={styles.userName}>
          {user.firstName} {user.lastName}
        </XLText>
      </View>
      <Content>
        <Form>
          <Controller
            control={control}
            render={({onChange, onBlur, value}) => (
              <DatePickerModal
                placeholder={Languages.birthDate}
                selectedDate={value}
                setSelectedDate={date => onChange(moment(date).format('L'))}
              />
            )}
            name="birthdate"
            defaultValue={initialState.birthdate}
          />
          <Item stackedLabel>
            <Label>{Languages.phone}</Label>
            <Controller
              control={control}
              render={({onChange, onBlur, value}) => (
                <Input
                  value={value}
                  onBlur={onBlur}
                  onChangeText={text => onChange(text)}
                />
              )}
              name="phone"
              defaultValue={initialState.phone}
            />
          </Item>
          <Item stackedLabel>
            <Label>{Languages.email}</Label>
            <Input value={user.email} editable={false} />
          </Item>
          <ListItem style={styles.passContainer}>
            <Left style={styles.left}>
              <Item stackedLabel style={styles.passItem}>
                <Label>{Languages.password}</Label>
                <Input editable={false} />
              </Item>
            </Left>
            <Right>
              <Icon
                name="chevron-right"
                type={GlobalStyles.Constants.iconType.materialCom}
                onPress={() => Linking.openURL('https://google.com')}
              />
            </Right>
          </ListItem>
          <Controller
            control={control}
            render={({onChange, onBlur, value}) => (
              <ListItem style={styles.passContainer}>
              <Item stackedLabel>
                <Label>{Languages.sex}</Label>
                <RNPickerSelect
                  style={pickerSelectStyles}
                  onValueChange={value => onChange(value)}
                  placeholder={{label: 'Select civility'}}
                  items={[
                    {key: "Mr", label: 'Mr', value: 'Mr'},
                    {key: "Mme",label: 'Mme', value: 'Mme'},
                    {key: "Other",label: 'Other', value: 'Other'},
                  ]}
                  value={value}
                />
              </Item>
              </ListItem>
            )}
            name="civility"
            defaultValue={initialState.civility}
          />
        </Form>
        <RegularText textStyle={styles.addressText}>Address</RegularText>
        <View style={styles.address}>
          <Form style={styles.addressInput}>
            <Item>
              <Label>{Languages.number}</Label>
              <Controller
                control={control}
                render={({onChange, onBlur, value}) => (
                  <Input
                    value={value}
                    onBlur={onBlur}
                    onChangeText={text => onChange(text)}
                  />
                )}
                name="number"
                defaultValue={initialState.number}
              />
            </Item>
            <Item>
              <Label>{Languages.street}</Label>
              <Controller
                control={control}
                render={({onChange, onBlur, value}) => (
                  <Input
                    value={value}
                    onBlur={onBlur}
                    onChangeText={text => onChange(text)}
                  />
                )}
                name="street"
                defaultValue={initialState.street}
              />
            </Item>
            <Item>
              <Label>{Languages.city}</Label>
              <Controller
                control={control}
                render={({onChange, onBlur, value}) => (
                  <Input
                    value={value}
                    onBlur={onBlur}
                    onChangeText={text => onChange(text)}
                  />
                )}
                name="city"
                defaultValue={initialState.city}
              />
            </Item>
            <Item>
              <Label>{Languages.zip}</Label>
              <Controller
                control={control}
                render={({onChange, onBlur, value}) => (
                  <Input value={value} onChangeText={text => onChange(text)} />
                )}
                name="zipCode"
                defaultValue={initialState.zipCode}
              />
            </Item>
          </Form>
        </View>
      </Content>
      <SolidButton
        buttonStyle={styles.save}
        title={Languages.save}
        onPress={handleSubmit(onPressSave)}
      />
    </Container>
  );
};
export default EditProfile;

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    width: Constants.screenWidth - 20 ,
    // borderWidth: 1,
    //  borderColor: 'transparent',
    // borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    width: Constants.screenWidth - 20,
    // borderWidth: 0.5,
    // borderColor: 'purple',
    // borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
