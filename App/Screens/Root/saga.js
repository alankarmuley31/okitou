import {call, put, takeLatest, select} from 'redux-saga/effects';
import Actions, {AppTypes} from './reducer';
import AlertApi from '../../Services/alert';
import {navigate} from '../../Utils/rNavigation';
import ProfileAction, {selectUser} from '../Profile/reducer';
import {setToken} from '../../Services/genericApi';

function* error({payload}) {
  yield put(Actions.loading(false));
  if (typeof payload === 'string') {
    yield put(Actions.showAlert(payload))
  } else {
    yield put(Actions.showAlert(payload.message))
  }
}

function* startup() {
  const {auth, auth_token} = yield select(selectUser);
  if (auth){
    if (auth_token) {
      yield call(setToken, auth_token);
    }
   // yield call(navigate, 'MainTab');

    yield call(navigate, 'TransportFlow');
  } else {
    yield call(navigate, 'Login');
  }
}

function* alert({payload}) {
  yield call(AlertApi.alert, payload);
}

export default [
  takeLatest(AppTypes.STARTUP, startup),
  takeLatest(AppTypes.ERROR, error),
  takeLatest(AppTypes.SHOW_ALERT, alert),
];
