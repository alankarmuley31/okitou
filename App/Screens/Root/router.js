import React, {useEffect} from 'react';
import {navigationRef, isMountedRef} from '../../Utils/rNavigation';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Icon} from 'react-native-elements';
import {GlobalStyles, Colors} from '@common';

// Auth Flow
import Introduction from '../Introduction';
import Login from '../Login';
import Register from '../Register';

// Main Tab

// HomeStack
import Home from '../Home';
import CategoryDetail from '../CategoryDetail';
// OrderStack
import Orders from '../Orders';

// ProfileStack
import Profile from '../Profile';
import EditProfile from '../EditProfile';
import ServiceFlow from '../ServiceFlow';
import TransportFlow from '../TransportFlow';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const headerOption = {
  headerStyle: {
    backgroundColor: Colors.red,
    shadowRadius: 0,
    elevation: 0,
    shadowOpacity: 0,
    shadowOffset: {
      height: 0,
    },
  },
  headerTintColor: Colors.black,
}

const homeStack = () => (
  <Stack.Navigator screenOptions={headerOption}>
    <Stack.Screen
      name="Home"
      component={Home}
      options={{title: '', headerLeft: null}}
    />
     <Stack.Screen
      name="CategoryDetail"
      component={CategoryDetail}
      options={{title: ''}}
    />
     <Stack.Screen
      name="ServiceFlow"
      component={ServiceFlow}
      options={{title: ''}}
    />
     <Stack.Screen
      name="TransportFlow"
      component={TransportFlow}
      options={{title: ''}}
    />
  </Stack.Navigator>
);

const orderStack = () => (
  <Stack.Navigator screenOptions={headerOption}>
    <Stack.Screen
      name="Orders"
      component={Orders}
      options={{title: '', headerLeft: null}}
    />
  </Stack.Navigator>
);

const profileStack = () => (
  <Stack.Navigator screenOptions={headerOption}>
    <Stack.Screen
      name="Profile"
      component={Profile}
      options={{title: '', headerLeft: null}}
    />
  </Stack.Navigator>
);

const createMainStack = () => (
  <Stack.Navigator
    screenOptions={headerOption}>
    <Stack.Screen
      name="TransportFlow"
      component={TransportFlow}
      options={{title: ''}}
    />
    <Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
    <Stack.Screen name="Register" component={Register} options={{headerShown: false}} />
    <Stack.Screen name="MainTab" children={createMainTab} options={{headerShown: false}} />
    <Stack.Screen
      name="EditProfile"
      component={EditProfile}
      options={{title: ''}}
    />
  </Stack.Navigator>
);

const createMainTab = () => (
  <Tab.Navigator
    screenOptions={({route}) => ({
      tabBarIcon: ({focused, color, size}) => {
        let icon;
        switch (route.name) {
          case 'Home':
            icon = 'home';
            break;
          case 'Orders':
            icon = 'format-list-bulleted';
            break;
          case 'Profile':
            icon = 'person';
            break;
        }
        // You can return any component that you like here!
        return (
          <Icon
            name={icon}
            type={GlobalStyles.Constants.iconType.material}
            color={focused ? Colors.red : Colors.black}
          />
        );
      },
    })}
    tabBarOptions={{
      activeTintColor: Colors.red,
      inactiveTintColor: Colors.black,
      style: {
        backgroundColor: Colors.offWhite,
      },
    }}>
    <Tab.Screen name="Home" component={homeStack} />
    <Tab.Screen name="Orders" component={orderStack} />
    <Tab.Screen name="Profile" component={profileStack} />
  </Tab.Navigator>
);

export default () => {
  useEffect(() => {
    isMountedRef.current = true;
    return () => (isMountedRef.current = false);
  }, []);
  return (
    <NavigationContainer ref={navigationRef}>
      {createMainStack()}
    </NavigationContainer>
  );
};
