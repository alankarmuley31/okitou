import { StyleSheet } from 'react-native';
import { Colors, Fonts, Constants } from '@common';

export default StyleSheet.create({
    containerStyle: {
        backgroundColor: Colors.offWhite
    },
    stepIndicatorView: {
        backgroundColor: Colors.red,
        paddingVertical: 10,
    },
    formView: {
        flex:1,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    nextButton: {
        backgroundColor: Colors.red,
    },
    backButton: {
        backgroundColor: Colors.lightGray,
    },
    bottonButtonView:{
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0
    }
})
