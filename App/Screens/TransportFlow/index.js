import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Container} from 'native-base';
import {View} from 'react-native';
import CourierActions from '../Home/reducer';
import TransportShippingElement from '../../Components/TransportShippingElement';
import Address from '../../Components/Address';
import Confirm from '../../Components/Confirm';
import styles from './styles';
import StepIndicator from 'react-native-step-indicator';
import {Constants} from '@common';

const TransportFlow = ({navigation}) => {
  const dispatch = useDispatch();
  const courier = useSelector(state => state.courier);
  const [stepperPosition, setStepperPosition] = useState(0);

  const renderSteps = () => {
    switch (stepperPosition) {
      case 0:
        return (
          <TransportShippingElement
            aCourier={courier.aCourier}
            onNext={data => {
              dispatch(CourierActions.updateCourier(data));
              setStepperPosition(stepperPosition + 1);
            }}
          />
        );
      case 1:
        return (
          <Address
            aCourier={courier.aCourier}
            updateCourier={data => {
              dispatch(CourierActions.updateCourier(data));
            }}
            onNext={() => {
              setStepperPosition(stepperPosition + 1);
            }}
            onBack={() => setStepperPosition(stepperPosition - 1)}
          />
        );
      case 2:
        return (
          <Address
            aCourier={courier.aCourier}
            updateCourier={data => {
              dispatch(CourierActions.updateCourier(data));
            }}
            onNext={() => {
              setStepperPosition(stepperPosition + 1);
            }}
            onBack={() => setStepperPosition(stepperPosition - 1)}
          />
        );
      case 3:
        return (
          <Confirm
            aCourier={courier.aCourier}
            onConfirm={() => {}}
            onBack={() => setStepperPosition(stepperPosition - 1)}
          />
        );
    }
  };

  return (
    <Container style={styles.containerStyle}>
      <View style={styles.stepIndicatorView}>
        <StepIndicator
          customStyles={Constants.stepperStyle}
          currentPosition={stepperPosition}
          labels={Constants.transportSteps}
          stepCount={4}
        />
      </View>
      <View style={styles.formView}>{renderSteps()}</View>
    </Container>
  );
};
export default TransportFlow;
