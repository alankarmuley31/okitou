import {call, put, takeLatest, select} from 'redux-saga/effects';
import Actions, {CourierTypes, courier} from './reducer';
import AppActions, {app} from '../Root/reducer';
import OkitouApi from '../../Services/OkitouApi';


function* getServicesCategories() {
    try {
        yield put(AppActions.loading(true));
        const {data} = yield call(OkitouApi.getServicesCategories, null );
        if (data) {
            yield put(Actions.setServicesCategories(data));
            yield put(AppActions.loading(false));
        }
    } catch (e) {
        yield put(AppActions.error(e));
    }
}

export default [
    takeLatest(CourierTypes.GET_SERVICES_CATEGORIES, getServicesCategories),
];