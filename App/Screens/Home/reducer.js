import {createActions, createReducer} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
    getServicesCategories: null,
    setServicesCategories: ['payload'],
    updateCourier: ['payload'],
    resetCourier: null,
});

export const CourierTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

const INITIAL_STATE = Immutable({
    aCourier: {},
    type:[
      {
        id: '1',
        name: 'Shopping',
      },
      {
        id: '2',
        name: 'Transport',
      },
      {
        id: '3',
        name: 'Services',
      },
    ],
    servicesCategories: [
        { "CategoryId" : 1,
          "CategoryName" : "Bank",
          "CategoryIcon" : "https://cdn.onlinewebfonts.com/svg/img_473842.png",
          "IsActive" : true,
        },
        { "CategoryId" : 2,
          "CategoryName" : "Post Office",
          "CategoryIcon" : "https://cdn.onlinewebfonts.com/svg/img_465546.png",
          "IsActive" : true,
        },
        { "CategoryId" : 3,
          "CategoryName" : "Hospital",
          "CategoryIcon" : "https://cdn.onlinewebfonts.com/svg/img_449023.png",
          "IsActive" : true,
        },
        { "CategoryId" : 4,
          "CategoryName" : "Pay Bill",
          "CategoryIcon" : "https://cdn.onlinewebfonts.com/svg/img_86734.png",
          "IsActive" : false,
        }
     ],
});

// /* ------------- Reducers ------------- */

const setServicesCategories = (state, {payload}) => state.set('servicesCategories', payload);
const updateCourier = (state, {payload}) => state.set('aCourier', {...state.aCourier, ...payload});
const resetCourier = (state, {payload}) => state.set('aCourier', {});
// /* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_SERVICES_CATEGORIES]: setServicesCategories,
  [Types.UPDATE_COURIER]: updateCourier,
  [Types.RESET_COURIER]: resetCourier,
});

export const courier = state => state.courier;
