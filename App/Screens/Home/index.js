import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {FlatList, View} from 'react-native';
import {Container, Content} from 'native-base';
import {GlobalStyles, Languages} from '@common';
import {XLText, RegularText} from '@Typography';
import CategoryItem from '../../Components/CategoryItem';
import styles from './styles';
import ProfileActions from '../Profile/reducer';

const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const courier = useSelector(state => state.courier);

  useEffect(() => {
    dispatch(ProfileActions.getProfile());
  });
  const onPressCategory = item => {
    dispatch(ProfileActions.getProfileCheckValidity({ "type": item}));
  };
  return (
    <Container>
      <View style={styles.header}>
        <XLText textStyle={styles.textStyle}>{Languages.need_anything}</XLText>
        <RegularText>{Languages.pick_category}</RegularText>
      </View>
      <CategoryItem
        title={courier.type[0].name}
        profileImage={courier.type[0].banner}
        onPress={() => onPressCategory(courier.type[0])}
      />
      <CategoryItem
        title={courier.type[1].name}
        profileImage={courier.type[1].banner}
        onPress={() => onPressCategory(courier.type[1])}
      />
      <CategoryItem
        title={courier.type[2].name}
        profileImage={courier.type[2].banner}
        onPress={() => onPressCategory(courier.type[2])}
      />
    </Container>
  );
};
export default Home;
