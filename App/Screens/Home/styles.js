import {StyleSheet} from 'react-native';
import {Colors, Fonts, Constants} from '@common';

export default StyleSheet.create({
    header: {
        paddingBottom: 10,
        paddingHorizontal: 30,
        backgroundColor: Colors.red,
        alignItems: 'flex-start',
        justifyContent: 'center',
        width: '100%', 
    },
    textStyle:{
        marginBottom: 5
    }
})
