import {AccessToken, LoginManager} from 'react-native-fbsdk';
import {Alert} from 'react-native';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import {firebase} from '@react-native-firebase/auth';
import AppActions from '../Screens/Root/reducer';
import {put} from 'redux-saga/effects';

// ...
// Calling the following function will open the FB login dialogue:
export const facebookLogin = function* facebookLogin(isGuest) {
  let credential;
  try {
    yield put(AppActions.loading(true));
    const result = yield LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);

    if (result.isCancelled) {
      // handle this however suites the flow of your app
      // throw new Error('User cancelled request');
      yield put(AppActions.loading(false));
      return false;
    }

    // console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`,);

    // get the access token
    const data = yield AccessToken.getCurrentAccessToken();

    if (!data) {
      // handle this however suites the flow of your app
      // throw new Error('Something went wrong obtaining the users access token');
      yield put(AppActions.loading(false));
      return false;
    }

    // create a new firebase credential with the token
    credential = firebase.auth.FacebookAuthProvider.credential(
      data.accessToken,
    );
    if (isGuest) {
      const firebaseUserCredential = yield firebase
        .auth()
        .currentUser.linkWithCredential(credential);
      yield put(AppActions.loading(false));
      return firebaseUserCredential.user;
    } else {
      // login with credential
      const firebaseUserCredential = yield firebase
        .auth()
        .signInWithCredential(credential);
      yield put(AppActions.loading(false));
      const idTokenResult = yield firebase.auth().currentUser.getIdTokenResult();
      console.log('User JWT: ', idTokenResult.token);
      return idTokenResult.token;
    }
  } catch (e) {
    if (e.code === 'auth/credential-already-in-use') {
      const firebaseUserCredential = yield firebase
        .auth()
        .signInWithCredential(credential);
      return firebaseUserCredential.user;
    } else if (e.code === 'auth/account-exists-with-different-credential') {
      yield put(AppActions.loading(false));
      Alert.alert('Correo electrónico ya asociado a otra cuenta.');
      return false;
    }
    console.error(e);
  }
  yield put(AppActions.loading(false));
  return false;
};

// Calling this function will open Google for login.
export const googleLogin = function* googleLogin(isGuest) {
  let credential;
  try {
    yield put(AppActions.loading(true));
    // add any configuration settings here:
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/userinfo.profile'],
      // webClientId: '466578727893-rvoho7k75vlcug76k4c6q9vf97apuon7.apps.googleusercontent.com',
      //offlineAccess: true,
    });
    if (yield GoogleSignin.isSignedIn()) {
      // yield GoogleSignin.revokeAccess();
      yield GoogleSignin.signOut();
    }

    yield GoogleSignin.hasPlayServices();
    yield GoogleSignin.signIn();
    const {accessToken, idToken} = yield GoogleSignin.getTokens();
    // create a new firebase credential with the token
    credential = firebase.auth.GoogleAuthProvider.credential(
      idToken,
      accessToken,
    );
    if (isGuest) {
      const firebaseUserCredential = yield firebase
        .auth()
        .currentUser.linkWithCredential(credential);
      yield put(AppActions.loading(false));
      console.log(firebaseUserCredential.user);
      return firebaseUserCredential.user;
    } else {
      // login with credential
      const firebaseUserCredential = yield firebase
        .auth()
        .signInWithCredential(credential);
      yield put(AppActions.loading(false));
      console.log(firebaseUserCredential.user);
      const idTokenResult = yield firebase.auth().currentUser.getIdTokenResult();
      console.log('User JWT: ', idTokenResult.token);
      return idTokenResult.token;
    }
  } catch (e) {
    yield put(AppActions.loading(false));
    if (e.code === statusCodes.SIGN_IN_CANCELLED) {
      return false;
    } else if (e.code === statusCodes.IN_PROGRESS) {
      console.error(e);
      // operation (e.g. sign in) is in progress already
    } else if (e.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      console.error(e);
      // play services not available or outdated
      return false;
    } else if (e.code === 'auth/credential-already-in-use') {
      const firebaseUserCredential = yield firebase
        .auth()
        .signInWithCredential(credential);
      return firebaseUserCredential.user;
    } else {
      console.error(e);
      Alert.alert(e.toString());
      return false;
    }
  }
  return false;
};

export const guestFirebaseLogin = function* guestLogin() {
  try {
    yield put(AppActions.loading(true));
    // login with  as a guest user
    const firebaseUserCredential = yield firebase.auth().signInAnonymously();
    yield put(AppActions.loading(false));
    return firebaseUserCredential.user;
  } catch (e) {
    yield put(AppActions.loading(false));
    console.error(e);
  }
  return false;
};
