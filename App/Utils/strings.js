export function decomposeFullName(input: string): string {
    const fullName = input || '';
    const result = {};
  
    if (fullName.length > 0) {
      const nameTokens =
        fullName.match(
          /[A-Za-zÁ-ÚÑÜ][a-zá-úñü]+|([aeodlsz]+\s+)+[A-ZÁ-ÚÑÜ][a-zá-úñü]+/g,
        ) || [];
  
      if (nameTokens.length > 3) {
        result.firstName = nameTokens.slice(0, 2).join(' ');
      } else {
        result.firstName = nameTokens.slice(0, 1).join(' ');
      }
  
      if (nameTokens.length > 2) {
        result.lastName = `${nameTokens
          .slice(-2, -1)
          .join(' ')} ${nameTokens.slice(-1).join(' ')}`;
      } else {
        result.lastName = nameTokens.slice(-1).join(' ');
      }
    }
  
    return result;
  }
  
  export function initialFromFullName(input) {
    var initials = input.match(/\b\w/g) || [];
    initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
    return initials;
  }
  
  export function convertModelToFormData(data = {}, form = null, namespace = '') {
    let files = {};
    let model = {};
    for (let propertyName in data) {
      if (
        data.hasOwnProperty(propertyName) &&
        data[propertyName] instanceof File
      ) {
        files[propertyName] = data[propertyName];
      } else {
        model[propertyName] = data[propertyName];
      }
    }
  
    model = JSON.parse(JSON.stringify(model));
    let formData = form || new FormData();
  
    for (let propertyName in model) {
      if (!model.hasOwnProperty(propertyName) || !model[propertyName]) continue;
      let formKey = namespace ? `${namespace}[${propertyName}]` : propertyName;
      if (model[propertyName] instanceof Date)
        formData.append(formKey, model[propertyName].toISOString());
      else if (model[propertyName] instanceof File) {
        formData.append(formKey, model[propertyName]);
      } else if (model[propertyName] instanceof Array) {
        model[propertyName].forEach((element, index) => {
          const tempFormKey = `${formKey}[${index}]`;
          if (typeof element === 'object')
            convertModelToFormData(element, formData, tempFormKey);
          else formData.append(tempFormKey, element.toString());
        });
      } else if (
        typeof model[propertyName] === 'object' &&
        !(model[propertyName] instanceof File)
      )
        convertModelToFormData(model[propertyName], formData, formKey);
      else {
        formData.append(formKey, model[propertyName].toString());
      }
    }
  
    for (let propertyName in files) {
      if (files.hasOwnProperty(propertyName)) {
        formData.append(propertyName, files[propertyName]);
      }
    }
    return formData;
  }
  
  export default {
    initialFromFullName,
    decomposeFullName,
    convertModelToFormData,
  };
  