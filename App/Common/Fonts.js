const type = {
    black: 'Raleway-Black'
}

const size = {
    xl: 30,
    large: 20,
    regular: 18,
    medium: 15,
    small: 12,
  };

const style = {
    lightHeader: {
      fontFamily: type.light,
      fontSize: size.regular,
    },
}


export default {
    type,
    size,
    style,
  };
  