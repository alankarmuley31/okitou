import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');
import Color from './Colors';
const Constants = {
    screenWidth: width,
    screenHeight: height,
    mcIcon:  `material-community`,
    mIcon: 'material',
    maxTextLength: 1000,
    serviceSteps: ['Choose Step', 'Choose Address', 'Confirm'],
    transportSteps: ['Choose Elements', 'Pickup     Address','Shipping Address','Confirm'],
    stepperStyle: {
      stepIndicatorSize: 25,
      currentStepIndicatorSize: 30,
      separatorStrokeWidth: 2,
      currentStepStrokeWidth: 3,
      stepStrokeCurrentColor: Color.blue,
      stepStrokeWidth: 3,
      stepStrokeFinishedColor: Color.blue,
      stepStrokeUnFinishedColor: '#aaaaaa',
      separatorFinishedColor: Color.blue,
      separatorUnFinishedColor: '#aaaaaa',
      stepIndicatorFinishedColor: Color.blue,
      stepIndicatorUnFinishedColor: '#ffffff',
      stepIndicatorCurrentColor: '#ffffff',
      stepIndicatorLabelFontSize: 13,
      currentStepIndicatorLabelFontSize: 13,
      stepIndicatorLabelCurrentColor: Color.blue,
      stepIndicatorLabelFinishedColor: '#ffffff',
      stepIndicatorLabelUnFinishedColor: '#aaaaaa',
      labelColor: 'black',
      labelSize: 13,
      currentStepLabelColor: Color.blue,
  }
  };
  
  export default Constants;