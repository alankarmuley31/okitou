export default {
    red: '#FA80B1',
    blue: '#5764F3',
    white: '#FFFFFF',
    offWhite: '#F5F5F5',
    semiTransparent: '#0000004C',
    black: '#000000',
    darkGray: '#333',
    mediumGray: '#666',
    lightGray: '#999',
}